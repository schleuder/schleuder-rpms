RPMs for schleuder
==================

This repository contains SPEC files and helper scripts to build RPMs for the following projects:

* schleuder
* schleuder-cli
* schleuder-web

At the moment we support the following distributions:

* EL 7 (CentOS / RHEL 7) using SCL

More RPM based targets are welcome to be added.

Available Repository
--------------------

The schleuder dev team maintains COPR builds for the supported distributions at [schleuder/schleuder](https://copr.fedorainfracloud.org/coprs/schleuder/schleuder/), which can be used as a repository for your schleuder installations.

You can use that repository by enabling it in yum / dnf (Requiring yum-plugin-copr or dnf-plugin-copr)

     yum copr enable schleuder/schleuder
     # or
     dnf copr enable schleuder/schleuder

There is also a COPR called [schleuder/schleuder-latest](https://copr.fedorainfracloud.org/coprs/schleuder/schleuder-latest/) that contains builds for the last successfull commit on master for the particular project. It can be used as source to get a latest packaged build of the current development activities. As this is ongoing activity it might not yet be ready.

Using built RPMS
----------------

You need to enable the SCL repository:

    yum install centos-release-scl

See fore more information https://wiki.centos.org/AdditionalResources/Repositories/SCL

Once you have a repository configured containing the schleuder packages (e.g. see available repository) it's pretty straight forward:

    yum install schleuder schleuder-cli

Afterwards you can follow https://schleuder.nadir.org/docs/#installation to configure and hook schleuder into your MTA.

schleuder-web includes mod_passenger as a dependency, as well as provides an example apache config in /usr/share/doc/schleuder-web-*/doc/passenger-schleuder-web.conf and is intended to run as a passenger application in production environments.

Note about SCL & vendoring dependencies
---------------------------------------

We are packaging the RPMs to be used with Software Collections, to use a newer Ruby version, as well as bundle most of the required gems into the package itself. This is contrary to what most distribution package requirements are stating, but is the best way on how to package a piece of software with different library version dependencies than the one available in the distribution itself.

The RPMs ship with wrapper for the standard binaries provided (e.g. schleuder or schleuder-cli) that make sure the SCL is loaded before calling the actual binary. These wrappers are installed in the usual path like /usr/bin.

Schleuder (or schleuder-cli) and all their vendored dependencies are placed under /opt/schleuder(-cli), while schleuder-web is placed under /var/www/schleuder-web and /usr/lib64/schleuder-web to match standard SELinux paths.

The schleuder package ships with an SELinux policy that confines schleuder into it's own type and allows MTAs to pass mails to schleuder.

Building the RPMS
-----------------

Locally building the rpms, require the binaries mock, spectool and rpmbuild to be available (On Fedora: `dnf install mock rpmdevtools rpm-build`). Once installed you can use the build helper `bin/build.sh` to build the RPMs:

    Usage: ./bin/build.sh project_to_build [git_commit]

Projects to build are schleuder, schleuder-cli and schleuder-web

The optional git_commit parameter allows you to build a project on a specific git_commit

The built RPMs will be available in `results/`.

Contributing
------------

To contribute please follow this workflow:

1. Talk to us! E.g. create an issue about your idea or problem.
2. Fork the repository and work in a meaningful named branch that is based off of our "master".
3. Commit in rather small chunks but don't split depending code across commits. Please write sensible commit messages.
4. If in doubt request feedback from us!
5. When finished create a merge request.


Thank you for your interest!


#!/bin/sh

main_dir=$(dirname $(dirname $(readlink -f $0)))

if [ ! -e $main_dir/copr_config ]; then
  echo "Can't find copr_config in $main_dir/copr_config !"
  exit 1
fi

# mind the order!
deps=(
  npth
  libgpg-error
  libgcrypt
  libassuan
  libksba
  gnupg2
  gpgme
)

srpmsdir=`mktemp -d`
$(dirname $(readlink -f $0))/build_fedora_deps_srpms.sh $srpmsdir

srpms=''

for dep in "${deps[@]}"; do
  srpms="${srpms} $(ls -1 ${srpmsdir}/*${dep}*.src.rpm)"
done

for repo in schleuder-latest schleuder; do
  echo "Building RPMs in copr ${repo}"
  copr --config $main_dir/copr_config build $repo $srpms
  if [ $? -gt 0 ]; then
    echo "Error while building RPMs in copr ${repo}. Aborting!"
    exit 1
  fi
  echo "Building in copr ${repo} done"
done
rm -rf $scratchdir
echo "Building done!"

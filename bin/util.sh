#!/bin/bash

rootdir=$(dirname $(dirname $(readlink -f $0)))

function echoerr {
  echo "$@" 1>&2
}

function usage {
  if [ ! -z "$1" ]; then
    echoerr "$@"
  fi
  echoerr "Usage: $0 project_to_build [git_commit]"
  exit 1
}

function failed {
  echo -e '\033[0;31mFAILED!\033[0m'
}
function ok {
  echo -e '\033[0;32mOK!\033[0m'
}

#!/bin/bash

source $(dirname $(readlink -f $0))/util.sh

for p in schleuder schleuder-cli schleuder-web; do
  echo "Building package ${p}"
  $(dirname $(readlink -f $0))/build.sh $p
  if [ $? -gt 0 ]; then
    echoerr "Aborting due to failure."
    exit 1
  fi
  echo "Done!"
  echo
done

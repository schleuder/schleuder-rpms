#!/bin/bash

basedir=$(dirname $(readlink -f $0))

source $basedir/build_srpm.sh

if [[ $? -gt 0  || ! -f $srpm ]]; then
  echo "Error while building SRPM"
  echo $srpm
  exit 1
fi

$basedir/build_rpm.sh $srpm $2

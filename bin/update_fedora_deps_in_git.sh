#!/bin/sh

deps=(
  npth
  libgpg-error
  libgcrypt
  libassuan
  libksba
  gnupg2
  gpgme
)

scratchdir=`mktemp -d`

git_user=$(git config user.name)
git_email=$(git config user.email)

for dep in "${deps[@]}"; do
  echo "Updating ${dep}..."
  git clone https://0xacab.org/schleuder/rpm-${dep}.git $scratchdir/${dep}
  cd $scratchdir/$dep
  git config user.name ${git_user:-schleuder merge user}
  git config user.email ${git_email:-team@schleuder.org}
  git remote add fedora https://src.fedoraproject.org/rpms/$dep
  git fetch fedora
  git merge fedora/master
  if [ $? -gt 0 ]; then
    echo "Error while merging ${dep} in $scratchdir/$dep . Resolve it manually"
    exit 1
  fi
  sed -i 's/%define scl_ruby .*/%define scl_ruby rh-ruby27/' *.spec
  sed -i 's/%global scl .*/%global scl rh-ruby27/' *.spec
  git commit -m "update ruby-scl" *.spec
  git push origin
  echo "Updating ${dep} done!"
  echo
done

#!/bin/sh

# mind the order!
deps=(
  npth
  libgpg-error
  libgcrypt
  libassuan
  libksba
  gnupg2
  gpgme
)

basedir=$(dirname $(readlink -f $0))
srpmsdir=`mktemp -d`
resultdir=`mktemp -d`
$(dirname $(readlink -f $0))/build_fedora_deps_srpms.sh $srpmsdir

srpms=''

for dep in "${deps[@]}"; do
  srpms="${srpms} $(ls -1 ${srpmsdir}/*${dep}*.src.rpm)"
done

source $(dirname $(readlink -f $0))/util.sh

echo "Building RPMs in mockchain ${resultdir}"
mockchain -r $rootdir/mock/epel-7-x86_64-schleuder.cfg -l $resultdir --tmp_prefix schleuder-build-$$ $srpms
if [ $? -gt 0 ]; then
  echo "Error while building RPMs in mockchain ${resultdir}. Aborting!"
  exit 1
fi
echo "Building in mockchain ${resultdir} done"
rm -rf $srpmsdir
echo "Building done!"

#!/bin/sh

main_dir=$(dirname $(dirname $(readlink -f $0)))

if [ ! -e $main_dir/copr_config ]; then
  echo "Can't find copr_config in $main_dir/copr_config !"
  exit 1
fi

repo=$1

if [ $repo != 'schleuder' ] && [ $repo != 'schleuder-latest' ]; then
  echo "USAGE: $0 schleuder|schleuder-latest project_to_build [git_commit]"
  exit 1
fi

shift

basedir=$(dirname $(readlink -f $0))

source $basedir/build_srpm.sh

if [[ $? -gt 0  || ! -f $srpm ]]; then
  echo "Error while building SRPM"
  echo $srpm
  exit 1
fi

echo "Building RPM in copr ${repo}"
copr --config $main_dir/copr_config build $repo $srpm
if [ $? -gt 0 ]; then
  echo "Error while building ${srpm} in copr ${repo}. Aborting!"
  exit 1
fi
echo "Building ${dep} in copr ${repo} done"

#!/bin/sh

# mind the order!
deps=(
  npth
  libgpg-error
  libgcrypt
  libassuan
  libksba
  gnupg2
  gpgme
)

if [ -z $1 ]; then
  outputdir=`mktemp -d`
else
  outputdir=$1
fi
[ -d $outputdir ] || mkdir -p $outputdir
if [ ! -d $outputdir ]; then
  echo "$outputdir does not exist! Aborting..."
  exit 1
fi
scratchdir=`mktemp -d`

for dep in "${deps[@]}"; do
  (echo $* | grep -q $dep) && continue
  echo "Building SRPM for ${dep}"
  git clone -q https://0xacab.org/schleuder/rpm-${dep}.git $scratchdir/$dep
  cd $scratchdir/$dep
  srpm=$(fedpkg --release epel7 --name ${dep} srpm)
  if [ $? -gt 0 ]; then
    echo "Error while building srpm:"
    echo "${srpm}"
    exit 1
  fi
  srpm_path=$(echo "${srpm}" | grep Wrote: | sed 's/^Wrote: //')
  if [ ! -f "${srpm_path}" ]; then
    echo "Can't find srpm ${srpm_path}"
    exit 1
  fi
  srpms="${srpms} ${srpm_path}"
done


mv $srpms $outputdir
echo "Output Dir: ${outputdir}"

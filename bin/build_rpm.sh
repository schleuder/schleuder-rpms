#!/bin/bash

source $(dirname $(readlink -f $0))/util.sh

mock=/usr/bin/mock

srpm=$1
git_commit=$2

if [ ! -x $mock ]; then
  echoerr "mock can't be found on your system"
  exit 1
fi

if [ ! -f $srpm ]; then
  usage "SRPM ${srpm} does not exist!"
fi
resultdir=$rootdir/results

echo -n "Building RPMs: "
[ -d $resultdir ] || mkdir $resultdir
if [ -z $git_commit ]; then
  output=$(mock -r $rootdir/mock/epel-7-x86_64-schleuder.cfg --resultdir=${resultdir} --rebuild $srpm 2>&1)
else
  output=$(mock --define "git_commit ${git_commit}" -r $rootdir/mock/epel-7-x86_64-schleuder.cfg --resultdir=${resultdir} --rebuild $srpm 2>&1)
fi

if [ $? -gt 0 ]; then
  failed
  echoerr "Error while building RPMs:"
  echoerr "${output}"
  echoerr
  echoerr "Logs available in ${resultdir}"
  exit 1
else
  ok
fi

echo "RPMs available in ${resultdir}"
echo "Finished!"

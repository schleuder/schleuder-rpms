#!/bin/bash

source $(dirname $(readlink -f $0))/util.sh

project=$1
git_commit=$2

[ -z $project ] && usage

specfile=$project/SPECS/$project.spec
if [ ! -f $specfile ]; then
  usage "No such specfile (${specfile}) found!"
fi

rpmbuild=/usr/bin/rpmbuild
spectool=/usr/bin/spectool

if [ ! -x $rpmbuild ]; then
  echoerr "rpmbuild can't be found on your system"
  exit 1
fi
if [ ! -x $spectool ]; then
  echoerr "spectool can't be found on your system"
  exit 1
fi
if [ ! -z $git_commit ]; then
  git=/usr/bin/git
  if [ ! -x $git ]; then
    echoerr "git can't be found on your system"
    exit 1
  fi
fi

if [ -z $git_commit ]; then
  echo -n "Downloading sources: "
  test -d ${rootdir}/${project}/SOURCES || mkdir -p ${rootdir}/${project}/SOURCES
  output=$(spectool -C ${rootdir}/${project}/SOURCES -g -S ${specfile} 2>&1)
else
  echo -n "Generating tarball from git commit: "
  [ -d tmp/$project ] && rm -rf tmp/$project
  version=$(git clone -q https://0xacab.org/schleuder/$project tmp/$project && cd $rootdir/tmp/$project && git reset -q --hard ${git_commit} && (([ -f ./lib/${project}/version.rb ] && ruby -r./lib/${project}/version.rb -e "puts eval('${project}'.split('-').collect{|s| s.capitalize }.join('') + '::VERSION')") || echo 0.0.9))
  if [ $? -gt 0 ]; then
    failed
    echoerr "Error while getting sources:"
    echoerr "${output}"
    exit 1
  fi
  gh_short=${git_commit:0:7}
  tarball_name="${project}-${version}-${gh_short}"
  sed -i "s/^Version:.*/Version:  ${version}/" $specfile
  output=$(cd $rootdir/tmp/$project && git archive --format tar.gz --prefix $tarball_name/ -o ${rootdir}/${project}/SOURCES/$tarball_name.tar.gz $git_commit)
fi
if [ $? -gt 0 ]; then
  failed
  echoerr "Error while getting sources:"
  echoerr "${output}"
  exit 1
else
  ok
fi

echo -n "Building SRPM: "
if [ -z $git_commit ]; then
  output=$($rpmbuild --define "_topdir ${rootdir}/${project}" --define "_sourcedir ${rootdir}/${project}/SOURCES" -bs $specfile 2>&1)
else
  output=$($rpmbuild --define "git_commit ${git_commit}" --define "_topdir ${rootdir}/${project}" --define "_sourcedir ${rootdir}/${project}/SOURCES" -bs $specfile 2>&1)
fi
if [ $? -gt 0 ]; then
  failed
  echoerr "Error while generating SRPM:"
  echoerr "${output}"
  exit 1
else
  ok
fi

srpm=$(echo "${output}" | grep -E '^Wrote: ' | sed 's/^Wrote: //')

echo "SRPM: ${srpm}"

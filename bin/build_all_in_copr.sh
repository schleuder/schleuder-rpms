#!/bin/bash

copr=$1
shift

if [ -z $copr ]; then
  echo "USAGE: $0 schleuder|schleuder-latest"
  exit 1
fi

source $(dirname $(readlink -f $0))/util.sh

for p in schleuder schleuder-cli schleuder-web; do
  echo "Building package ${p}"
  $(dirname $(readlink -f $0))/build_in_copr.sh $copr $p
  if [ $? -gt 0 ]; then
    echoerr "Aborting due to failure."
    exit 1
  fi
  echo "Done!"
  echo
done


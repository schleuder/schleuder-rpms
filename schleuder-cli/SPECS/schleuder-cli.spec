%define scl_ruby rh-ruby27

# git based releases
%if 0%{?git_commit:1}
%define checkout %(date +%%Y%%m%%d)
%define git_short %(c=%{git_commit}; echo ${c:0:7})
%define release_ext .%{checkout}git%{git_short}
%define tar_dir %{name}-%{version}-%{git_short}
%define tarball_source %{name}-%{version}-%{git_short}.tar.gz
%else
%define release_ext %{nil}
%define tar_dir %{name}-%{version}
%define tarball_source https://0xacab.org/schleuder/%{name}/raw/master/gems/%{name}-%{version}.tar.gz
%endif

Name:     schleuder-cli
Version:  0.1.0
Release:  4%{release_ext}%{?dist}
Summary:  A command line tool to create and manage schleuder-lists.

Group:    Mail/Internet
License:  GPLv3
URL:      https://schleuder.nadir.org/
Source0:  %{tarball_source}

BuildRequires: %{scl_ruby}-rubygem-bundler
BuildRequires: %{scl_ruby}-ruby-devel
Requires: %{scl_ruby}-rubygem-bundler
Requires: %{scl_ruby}-ruby-devel

%description

Schleuder-cli enables creating, configuring, and deleting lists, subscriptions, keys, etc. It uses the Schleuder API, provided by schleuder-api-daemon (part of Schleuder).
Authentication and TLS-verification are mandatory. You need an API-key and the fingerprint of the TLS-certificate of the Schleuder API, respectively. Both should be provided by the API operators.
schleuder-cli does not authorize access. Only people who are supposed to have full access to all lists should be allowed to use it on/with your server.


%prep
%autosetup -p1 -n %{tar_dir}

# don't depend on git
sed -i 's/git ls-files \(.*\)`.split/find \1 -type f`.split/' %{name}.gemspec
sed -i 's/man lib README.md/lib/' %{name}.gemspec

%build
source /opt/rh/%{scl_ruby}/enable
rm Gemfile.lock
bundle lock
bundle install --path=bundler --standalone


%install
install -d -m 755 %{buildroot}/%{_mandir}/man1
cp man/%{name}*8 %{buildroot}/%{_mandir}/man1/

install -d        %{buildroot}/opt/%{name}
cp -a .bundle bundler bin lib spec Gemfile* Rakefile %{name}.gemspec %{buildroot}/opt/%{name}/

install -d -m 755 %{buildroot}/%{_bindir}
cat >%{buildroot}/%{_bindir}/%{name}<<END
#!/bin/bash
source /opt/rh/%{scl_ruby}/enable
cd /opt/%{name}/ && bundle exec %{name} "\$@"
END

%files
%doc *.md
%doc %attr(0644,root,root) %{_mandir}/man1/%{name}*
%license LICENSE.txt

%attr(0755,root,root) %{_bindir}/%{name}
/opt/%{name}



%changelog

* Sun Nov 19 2017 schleuder dev team <schleuder@nadir.org> - 0.1.0-3
- new ruby dep
* Sun Nov 19 2017 schleuder dev team <schleuder@nadir.org> - 0.1.0-2
- new ruby dep
* Fri Jul 07 2017 schleuder dev team <schleuder@nadir.org> - 0.1.0-1
- Initial release

%define scl_ruby rh-ruby27

%global selinuxtype targeted
%global moduletype services

# gitlab ticketing
%{!?_with_gitlab_ticketing: %{!?_without_gitlab_ticketing: %define _without_gitlab_ticketing --without-gitlab-ticketing}}
%{?_with_gitlab_ticketing: %{?_without_gitlab_ticketing: %{error: both _with_gitlab_ticketing and _without_gitlab_ticketing}}}
%if %{?_with_gitlab_ticketing:1}%{!?_with_gitlab_ticketing:0}
# unless properly released
%define gitlab_ticketing 1
%else
%define gitlab_ticketing 0
%endif
%define gitlab_ticketing_version 1.0.0

# git based releases
%if 0%{?git_commit:1}
%define checkout %(date +%%Y%%m%%d)
%define git_short %(c=%{git_commit}; echo ${c:0:7})
%define release_ext .%{checkout}git%{git_short}
%define tar_dir %{name}-%{version}-%{git_short}
%define tarball_source %{name}-%{version}-%{git_short}.tar.gz
%else
%define release_ext %{nil}
%define tar_dir %{name}-%{version}
%define tarball_source https://schleuder.org/download/%{name}-%{version}.tar.gz
%endif

Name:     schleuder
Version:  4.0.1
Release:  1%{release_ext}%{?dist}
Summary:  Schleuder is a gpg-enabled mailing list manager with resending-capabilities.

Group:    Mail/Internet
License:  GPLv3
URL:      https://schleuder.nadir.org/
Source0:  %{tarball_source}
Source1:  %{name}-api-daemon.service
Source2:  %{name}.cron.weekly
Source10:  %{name}.te
Source11:  %{name}.fc
Source12:  %{name}.if
Patch0:   0001-fix-paths.patch
Patch1:   0002-add-deps.patch

BuildRequires: %{scl_ruby}-rubygem-bundler
BuildRequires: %{scl_ruby}-ruby-devel
BuildRequires: %{scl_ruby}-gpgme >= 1.12.0
BuildRequires: gcc-c++
BuildRequires: texinfo
BuildRequires: chrpath
BuildRequires: systemd-units
BuildRequires: systemd
BuildRequires: sqlite-devel
BuildRequires: mariadb-devel
BuildRequires: postgresql-devel
BuildRequires: selinux-policy-devel
BuildRequires: libicu-devel
BuildRequires: which
Requires: %{scl_ruby}-rubygem-bundler
Requires: %{scl_ruby}-ruby-devel
Requires: %{scl_ruby}-gpgme >= 1.12.0
Requires: crontabs
Requires: selinux-policy
Requires: libicu
Requires(pre): shadow-utils
Requires(post): systemd
Requires(post): selinux-policy-base
Requires(post): policycoreutils
%if 0%{?fedora}
Requires(post): policycoreutils-python-utils
%else
Requires(post): policycoreutils-python
%endif
Requires(post): libselinux-utils
Requires(preun): systemd
Requires(postun): systemd

%description

Schleuder is a gpg-enabled mailing list manager with resending-capabilities. Subscribers can communicate encrypted (and pseudonymously) among themselves, receive emails from non-subscribers and send emails to non-subscribers via the list.


%prep
%autosetup -p1 -n %{tar_dir}

# don't depend on git
sed -i 's/s.files\(\s*\)= `git ls-files \(.*\)`.split/s.files\1= `find \2 -type f`.split/' %{name}.gemspec
# remove etc/ and README.md from the default gem location
sed -i 's/etc db README.md/db/' %{name}.gemspec


%build
source /opt/rh/%{scl_ruby}/enable

%if 0%{?gitlab_ticketing}
# unless it is released
echo "gem 'schleuder-gitlab-ticketing', '%{gitlab_ticketing_version}'" >> Gemfile
%endif

bundle lock
bundle install --path=bundler --standalone --without development

mkdir selinux; cd selinux
cp %{SOURCE10} %{SOURCE11} %{SOURCE12} .
sed -i 's@SCL@/opt/rh/%{scl_ruby}/root@' *.fc
make -f %{_datadir}/selinux/devel/Makefile
bzip2 -9 %{name}.pp

%install

install -d -m 750 %{buildroot}/%{_sysconfdir}/%{name}/plugins
install -d -m 750 %{buildroot}/%{_sysconfdir}/%{name}/filters/{post,pre}_decryption
install -d        %{buildroot}/%{_sysconfdir}/cron.weekly
install -d        %{buildroot}/%{_localstatedir}/log/%{name}/lists
install -d        %{buildroot}/%{_localstatedir}/lib/%{name}/lists
install -d -m 755 %{buildroot}/%{_unitdir}
install -d -m 755 %{buildroot}/%{_bindir}

install -p -m 644 %{SOURCE1} %{buildroot}/%{_unitdir}/%{name}-api-daemon.service
install -p -m 755 %{SOURCE2} %{buildroot}/%{_sysconfdir}/cron.weekly/%{name}

cp etc/*.yml %{buildroot}/%{_sysconfdir}/%{name}/

%if 0%{?gitlab_ticketing}
mv bundler/ruby/*/gems/schleuder-gitlab-ticketing-%{gitlab_ticketing_version}/lib/schleuder/filters/post_decryption/99_gitlab_ticketing.rb lib/schleuder/filters/post_decryption/99_gitlab_ticketing.rb
%endif

install -d -m 755 %{buildroot}/%{_mandir}/man1
cp man/%{name}*8 %{buildroot}/%{_mandir}/man1/

install -d        %{buildroot}/opt/%{name}
sed -i 's@remote: file:.*@remote: file:/opt/schleuder/gem_src/@' Gemfile.lock
cp -a .bundle bundler bin db lib locales spec Gemfile* Rakefile %{name}.gemspec %{buildroot}/opt/%{name}/

cat >%{buildroot}/%{_bindir}/%{name}<<END
#!/bin/bash
source /opt/rh/%{scl_ruby}/enable
cd /opt/%{name}/ && bundle exec %{name} "\$@"
END

cat >%{buildroot}/%{_bindir}/%{name}-api-daemon<<END
#!/bin/bash
source /opt/rh/%{scl_ruby}/enable
cd /opt/%{name}/ && bundle exec %{name}-api-daemon "\$@"
END

cd selinux
install -d %{buildroot}%{_datadir}/selinux/packages
install -d -p %{buildroot}%{_datadir}/selinux/devel/include/services
install -p -m 644 %{name}.if %{buildroot}%{_datadir}/selinux/devel/include/services
install -m 0644 %{name}.pp.bz2 %{buildroot}%{_datadir}/selinux/packages

chrpath -d  %{buildroot}/opt/%{name}/bundler/ruby/*/extensions/x86_64-linux/2.7.0/pg-*/pg_ext.so
chrpath -d  %{buildroot}/opt/%{name}/bundler/ruby/*/gems/pg-*/ext/pg_ext.so

%pre
getent group %{name} >/dev/null || groupadd -r %{name}
getent passwd %{name} >/dev/null || \
    useradd -r -g %{name} -d %{_localstatedir}/lib/%{name} -s /sbin/nologin \
    -c "%{name} GPG-enabled mailing list manager" %{name}
exit 0

%post
%systemd_post schleuder-api-daemon.service


%{_sbindir}/semodule -n -s %{selinuxtype} -r %{name} 2> /dev/null
%{_sbindir}/semodule -n -X 200 -s %{selinuxtype} -i /usr/share/selinux/packages/%{name}.pp.bz2 > /dev/null

if %{_sbindir}/selinuxenabled ; then
  %{_sbindir}/load_policy
  if [ $1 -eq 1 ]; then
    %{_sbindir}/semanage port --add -t schleuder_port_t -p tcp 4443
    /sbin/fixfiles -R %{name} restore || :
    /sbin/restorecon -R /opt/%{name} %{_localstatedir}/log/%{name} %{_localstatedir}/lib/%{name} || :
  fi
fi

%preun
%systemd_preun schleuder-api-daemon.service

%postun
%systemd_postun_with_restart schleuder-api-daemon.service

if [ $1 -eq 0 ]; then
  semanage port --list  | grep -E '^schleuder_port_t' | sed 's/[a-z _]*//' | while read port; do
    %{_sbindir}/semanage port --delete -t schleuder_port_t -p tcp $port
  done
  %{_sbindir}/semodule -n -r %{name} &> /dev/null || :
  if %{_sbindir}/selinuxenabled ; then
    %{_sbindir}/load_policy
  fi
fi


%files
%doc *.md
%doc %attr(0644,root,root) %{_mandir}/man1/%{name}*
%license LICENSE.txt

%dir %attr(0750,root,%{name}) %{_sysconfdir}/%{name}
%dir %attr(0750,root,%{name}) %{_sysconfdir}/%{name}/plugins
%dir %attr(0750,root,%{name}) %{_sysconfdir}/%{name}/filters
%{_unitdir}/%{name}-api-daemon.service
%attr(0755,root,root) %{_bindir}/%{name}*
%config(noreplace) %attr(0640,root,%{name}) %{_sysconfdir}/%{name}/*.yml
%config(noreplace) %{_sysconfdir}/cron.weekly/%{name}
%dir %attr(0750,%{name},%{name}) %{_localstatedir}/log/%{name}
%dir %attr(0750,%{name},%{name}) %{_localstatedir}/log/%{name}/lists
%dir %attr(0750,%{name},%{name}) %{_localstatedir}/lib/%{name}
%dir %attr(0750,%{name},%{name}) %{_localstatedir}/lib/%{name}/lists

%{_datadir}/selinux/*

/opt/%{name}



%changelog
* Mon Mar 08 2021 schleuder dev team <team@schleuder.org> - 4.0.0-1
- New release

* Tue Feb 09 2021 schleuder dev team <team@schleuder.org> - 3.6.0-1
- New release

* Mon Sep 16 2019 schleuder dev team <team@schleuder.org> - 3.4.1-1
- New release, switch to ruby 2.6

* Thu Feb 21 2019 schleuder dev team <team@schleuder.org> - 3.4.0-1
- New release, switch to gpg 2.2

* Wed Feb 07 2018 schleuder dev team <schleuder@nadir.org> - 3.2.2-1
- New release

* Sun Nov 26 2017 schleuder dev team <schleuder@nadir.org> - 3.2.1-4
- Build with gpg 2.2

* Thu Nov 23 2017 schleuder dev team <schleuder@nadir.org> - 3.2.1-3
- Add quickfix for ruby-mail

* Tue Nov 21 2017 schleuder dev team <schleuder@nadir.org> - 3.2.1-2
- Update to 3.2.1

* Fri Jul 07 2017 schleuder dev team <schleuder@nadir.org> - 3.1.1-1
- Initial release

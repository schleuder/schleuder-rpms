%define scl_ruby rh-ruby27

# git based releases
%if 0%{?git_commit:1}
%define checkout %(date +%%Y%%m%%d)
%define git_short %(c=%{git_commit}; echo ${c:0:7})
%define release_ext .%{checkout}git%{git_short}
%define tar_dir %{name}-%{git_short}
%define tarball_source https://0xacab.org/schleuder/%{name}/-/archive/%{git_short}/%{name}-%{git_short}.tar.gz
%else
# there is now official release atm
%define git_commit2 a2888f65a1f0302e71ab7c5c487f927deb914a5d
%define checkout %(date +%%Y%%m%%d)
%define git_short %(c=%{git_commit2}; echo ${c:0:7})
%define release_ext .%{checkout}git%{git_short}
%define tar_dir %{name}-%{git_short}
%define tarball_source https://0xacab.org/schleuder/%{name}/-/archive/%{git_short}/%{name}-%{git_short}.tar.gz
%endif

%global selinuxtype targeted
%global moduletype services

Name:     schleuder-web
Version:  0.0.18
Release:  1%{release_ext}%{?dist}
Summary:  Schleuder Web is a web-interface to administrate Schleuder-lists.
Group:    Mail/Internet
License:  GPLv3
URL:      https://schleuder.nadir.org/
Source0:  %{tarball_source}
Source1:  %{name}.logrotate
Source10:  %{name}.te
Source11:  %{name}.fc
Source12:  %{name}.if

BuildRequires: %{scl_ruby}-rubygem-bundler
BuildRequires: %{scl_ruby}-ruby-devel
BuildRequires: chrpath
BuildRequires: libxml2-devel
BuildRequires: zlib-devel
BuildRequires: httpd
BuildRequires: sqlite-devel
BuildRequires: mariadb-devel
BuildRequires: postgresql-devel
BuildRequires: selinux-policy-devel
BuildRequires: autotools-latest-automake
Requires: %{scl_ruby}-rubygem-bundler
Requires: %{scl_ruby}-ruby-devel
Requires: mod_passenger
Requires: logrotate
Requires: selinux-policy
Requires(pre): shadow-utils
Requires(post): selinux-policy-base
Requires(post): policycoreutils
%if 0%{?fedora}
Requires(post): policycoreutils-python-utils
%else
Requires(post): policycoreutils-python
%endif
Requires(post): libselinux-utils

%description

schleuder-web is a full featured web interface to administrate Schleuder v3-lists and subscriptions.

%prep
%autosetup -n %{tar_dir}

%build
source /opt/rh/%{scl_ruby}/enable
export PATH=$PATH:/opt/rh/autotools-latest/root/usr/bin/
export LD_LIBRARY_PATH=/opt/rh/autotools-latest/root/usr/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}

# extend activerecord support
echo "gem 'activerecord-postgresql-adapter'" >> Gemfile
echo "gem 'pg', '~> 0.21'" >> Gemfile
echo "gem 'activerecord-mysql2-adapter'" >> Gemfile

sed -i "/^BUNDLED WITH/{n;s/.*/    $(rpm -q --queryformat "%{VERSION}" %{scl_ruby}-rubygem-bundler)/}" Gemfile.lock
bundle lock
bundle install --path=bundler --standalone --without development

bundle exec rake assets:precompile RAILS_ENV=production SECRET_KEY_BASE=$(bundle exec rake secret)

# we move the bundle vendor to lib for correct selinux context
sed -i 's@BUNDLE_PATH:.*@BUNDLE_PATH: %{_libdir}/%{name}@' .bundle/config


cat >doc/passenger-%{name}.conf<<END
Alias /schleuder %{_localstatedir}/www/schleuder-web/public
<Location /schleuder>
    PassengerBaseURI /schleuder
    PassengerAppRoot %{_localstatedir}/www/schleuder-web

    # mandatory
    #SetEnv SECRET_KEY_BASE ENABLE_AND_SET_ME_TO_WORK
    #SetEnv SCHLEUDER_TLS_FINGERPRINT GET_IT_FROM_API_DAEMON
    #SetEnv SCHLEUDER_API_KEY GET_IT_FROM_API_DAEMON

    # Optional
    #SetEnv SCHLEUDER_API_HOST localhost
    #SetEnv SCHLEUDER_API_PORT 4443

    # don't touch
    SetEnv GEM_HOME %{_localstatedir}/www/schleuder-web/.bundle
    SetEnv GEM_PATH %{_localstatedir}/www/schleuder-web/.bundle

    SetEnv PATH /opt/rh/%{scl_ruby}/root/usr/local/bin:/opt/rh/%{scl_ruby}/root/usr/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin
    SetEnv LD_LIBRARY_PATH /opt/rh/%{scl_ruby}/root/usr/local/lib64:/opt/rh/%{scl_ruby}/root/usr/lib64
    SetEnv MANPATH /opt/rh/%{scl_ruby}/root/usr/local/share/man:/opt/rh/%{scl_ruby}/root/usr/share/man:
    SetEnv PKG_CONFIG_PATH /opt/rh/%{scl_ruby}/root/usr/local/lib64/pkgconfig:/opt/rh/%{scl_ruby}/root/usr/lib64/pkgconfig
    SetEnv XDG_DATA_DIRS /opt/rh/%{scl_ruby}/root/usr/local/share:/opt/rh/%{scl_ruby}/root/usr/share:/usr/local/share:/usr/share
    PassengerRuby /opt/rh/%{scl_ruby}/root/usr/bin/ruby
</Location>
<Directory %{_localstatedir}/www/schleuder-web/public>
    Options -MultiViews
    Require all granted
</Directory>
END

touch db/production.sqlite3

mkdir selinux; cd selinux
cp %{SOURCE10} %{SOURCE11} %{SOURCE12} .
make -f %{_datadir}/selinux/devel/Makefile
bzip2 -9 %{name}.pp

%install

install -d        %{buildroot}%{_libdir}
install -d        %{buildroot}%{_sysconfdir}/logrotate.d
install -d        %{buildroot}%{_localstatedir}/www/%{name}/tmp
install -d        %{buildroot}%{_localstatedir}/www/%{name}/log

cp -a %{SOURCE1}  %{buildroot}%{_sysconfdir}/logrotate.d/%{name}
cp -a bundler     %{buildroot}%{_libdir}/%{name}
cp -a app bin .bundle config* db Gemfile* lib public Rakefile %{buildroot}%{_localstatedir}/www/%{name}/

cd selinux
install -d %{buildroot}%{_datadir}/selinux/packages
install -d -p %{buildroot}%{_datadir}/selinux/devel/include/services
install -p -m 644 %{name}.if %{buildroot}%{_datadir}/selinux/devel/include/services
install -m 0644 %{name}.pp.bz2 %{buildroot}%{_datadir}/selinux/packages

chrpath -d  %{buildroot}%{_libdir}/%{name}/ruby/*/extensions/x86_64-linux/2.7.0/pg-*/pg_ext.so
chrpath -d  %{buildroot}%{_libdir}/%{name}/ruby/*/gems/pg-*/ext/pg_ext.so
chrpath -d  %{buildroot}%{_libdir}/%{name}/ruby/*/gems/nokogiri-*/lib/nokogiri/*/nokogiri.so
%pre
getent group %{name} >/dev/null || groupadd -r %{name}
getent passwd %{name} >/dev/null || \
    useradd -r -g %{name} -d %{_localstatedir}/www/%{name} -s /sbin/nologin \
    -c "%{name} GPG-enabled mailing list manager webfronted" %{name}
exit 0

%post

%{_sbindir}/semodule -n -s %{selinuxtype} -r %{name} 2> /dev/null
%{_sbindir}/semodule -n -X 200 -s %{selinuxtype} -i /usr/share/selinux/packages/%{name}.pp.bz2 > /dev/null

if %{_sbindir}/selinuxenabled ; then
  %{_sbindir}/load_policy
fi

/sbin/fixfiles -R %{name} restore || :
/sbin/restorecon -R %{_localstatedir}/www/%{name} || :

touch %{_localstatedir}/www/%{name}/tmp/restart.txt


%postun
if [ $1 -eq 0 ]; then
  %{_sbindir}/semodule -n -r %{name} &> /dev/null || :
  if %{_sbindir}/selinuxenabled ; then
    %{_sbindir}/load_policy
    /sbin/fixfiles -R myapp restore || :
    [ -d %{_localstatedir}/www/%{name} ]  && \
      /sbin/restorecon -R %{_localstatedir}/www/%{name} &> /dev/null || :
  fi
fi


%files
%doc *.md
%doc doc
%license LICENSE.txt

%config(noreplace) %attr(0644,root,root) %{_sysconfdir}/logrotate.d/%{name}
%config(noreplace) %attr(0640,root,%{name}) %{_localstatedir}/www/%{name}/config/*.yml
%dir %attr(0750,%{name},root) %{_localstatedir}/www/%{name}/db
%config(noreplace) %attr(0640,%{name},%{name}) %{_localstatedir}/www/%{name}/db/production.sqlite3
%attr(0640,%{name},%{name}) %{_localstatedir}/www/%{name}/config.ru
%dir %attr(0750,%{name},%{name}) %{_localstatedir}/www/%{name}/tmp
%dir %attr(0770,%{name},apache) %{_localstatedir}/www/%{name}/log

%{_datadir}/selinux/*

%{_localstatedir}/www/%{name}
%{_libdir}/%{name}

%changelog

* Fri Jul 07 2017 schleuder dev team <schleuder@nadir.org> - 0.0.9-1
- Initial release
